<?php
/*
Plugin Name: WPezPlugins: OSM (OpenStreetMap.org)
Plugin URI: TODO
Description: A WordPress plugin that uses two shortcodes to render OpenStreetMap.org maps using the OpenLayers.org JavaScript library.
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: http://AlchemyUnited.com
License: GPLv2+
Text Domain: wpez-osm
Domain Path: /languages
*/

namespace WPezOSM;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezOSM\App\ClassPlugin;

$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('This plugin - namespace: ' . __NAMESPACE__ . ' - requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}


function autoloader( $bool = true ){

    if ( $bool !== true ) {
        return;
    }

    require_once 'App/Core/Autoload/ClassWPezAutoload.php';

    $new_autoload = new ClassWPezAutoload();
    $new_autoload->setPathParent( dirname( __FILE__ ) );
    $new_autoload->setNeedle(__NAMESPACE__ );
    $new_autoload->setReplaceSearch(__NAMESPACE__ . '\\');

    spl_autoload_register( [$new_autoload, 'WPezAutoload'], true );
}
autoloader();

function plugin($bool = true){

    if ( $bool !== true ) {
        return;
    }

    $new_plugin = new ClassPlugin();
}
plugin();