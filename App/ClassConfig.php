<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 07-Jun-18
 * Time: 5:28 AM
 */

namespace WPezOSM\App;

if ( ! class_exists( '\WPezOSM\App\ClassConfig' ) ) {

    class ClassConfig {

        protected $_arr_allowed_html;
        protected $_str_img_pin;
        protected $_arr_marker_icon_sizes;
        protected $_str_sc_name;
        protected $_str_sc_a11y_slug ;
        protected $_str_sc_method;
        protected $_arr_scripts;
        protected $_str_scripts_ver;
        protected $_arr_styles;


        public function __construct() {

            $this->setPropertyDefaults();
        }

        protected function setPropertyDefaults() {

            $this->_arr_allowed_html = [
                'br'     => [],
                'em'     => [],
                'strong' => [],
                'b'      => []
            ];

            $this->_str_img_pin = plugin_dir_url( __FILE__ ) . 'assets/dist/svg/map-pin.min.svg';

            $this->_arr_marker_icon_sizes = [

                'xs' => '25px',
                'sm' => '30px',
                'md' => '35px',
                'lg' => '50px',
                'xl' => '75px'
            ];

            $this->_str_sc_name   = 'wpez_osm';
            $this->_str_sc_a11y_slug  = '_a11y';
            $this->_str_sc_method   = 'scOSM';

          //  $str_ver = 'v_4.6.5';
            $str_ver = 'v_i3';

            $this->_arr_scripts[] = [
                'handle'    => 'ol_polyfill',
                'src'       => 'https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList',
                'deps'      => [],
                'ver'       => $str_ver,
                'in_footer' => true,
            ];

            $this->_arr_scripts[] = [
                'handle'    => 'openlayers_js',
                'src'       => 'https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.js',
                'deps'      => [ 'ol_polyfill' ],
                'ver'       => $str_ver,
                'in_footer' => true,
            ];

            $this->_arr_scripts[] = [
                'handle'    => 'wpez_osm',   // IMPORTANT - this handle MUST match the handle in the wp localize script (in the plugin.php
                'src'       => plugin_dir_url( __FILE__ ) . 'assets/dist/js/wpez-osm.min.js',
                'deps'      => [ 'openlayers_js' ],
                'ver'       => $str_ver,
                'in_footer' => true,
            ];

            // styles
            $this->_arr_styles[] = [

                'handle' => 'openlayers_css',
                'src'    => 'https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.css',
                'deps'   => [],
                'ver'    => $str_ver,
                'media'  => 'all',

            ];
        }

        public function __get( $str_prop = false ) {

            $str_prop = strtolower( $str_prop );

            switch ( $str_prop ) {

                case 'a11y_slug':
                    return $this->_str_sc_a11y_slug;

                case 'allowed_html':
                    return $this->_arr_allowed_html;
                    
                case 'img_pin':
                    return $this->_str_img_pin;

                case 'marker_icon_sizes':
                    return  $this->_arr_marker_icon_sizes;

                case 'sc_method':
                    return $this->_str_sc_method;

                case 'sc_name':
                    return $this->_str_sc_name;

                case 'scripts':
                    return $this->_arr_scripts;

                case 'styles':
                    return $this->_arr_styles;

                default:
                    return '';

            }

        }

    }
}