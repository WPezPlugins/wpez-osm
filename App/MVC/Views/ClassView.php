<?php
/**
 * Created by PhpStorm.
 */

namespace WPezOSM\App\MVC\Views;

class ClassView {

    use \WPezOSM\App\Core\Traits\Set\TraitSetString;
    use \WPezOSM\App\Core\Traits\Set\TraitSetBool;

    protected $_str_id;
    protected $_str_class;
    protected $_str_text; // for label, title and footer
    protected $_str_icon;
    protected $_str_icon_color;

    protected $_arr_stash_markers;
    protected $_arr_stash_labels;
    protected $_bool_stash_style;
    protected $_arr_stash_style;

    protected $_bool_a11y;
    protected $_bool_a11y_add_style;
    protected $_str_a11y_id_slug;
    protected $_str_a11y_title;
    protected $_str_a11y_id;
    protected $_str_a11y_class;
    protected $_str_a11y;
    protected $_str_wrapper;
    protected $_str_title;
    protected $_str_footer;

    protected $_arr_allowed_html;
    protected $_str_img_pin;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->resetPropertyDefaults();
        $this->resetMapPropertyDefaults();

        $this->_arr_allowed_html = [
            'br' => [],
        ];
        $this->_str_img_pin      = false;

        $this->_bool_a11y_add_style = '.zzz{display: block}';

        $this->_str_a11y_id_slug = '-a11y';
        $this->_str_a11y_class   = 'screen-reader-text';
    }

    protected function resetPropertyDefaults() {

        $this->_str_id         = false;
        $this->_str_class      = false;
        $this->_str_text       = false;
        $this->_str_icon       = false;
        $this->_str_icon_color = false;
    }

    protected function resetMapPropertyDefaults() {

        $this->_str_text       = false; // TODO - this is also above.

        $this->_arr_stash_markers = [];
        $this->_arr_stash_labels  = [];
        $this->_arr_stash_style   = [];
        $this->_bool_stash_style  = true;
        $this->_arr_stash_style[] = ' svg .wpez-osm-svg-outer{' . 'fill: #ffffff' . '}';

        $this->_bool_a11y = false;

        $this->_str_a11y    = false;
        $this->_str_a11y_id = false;
        $this->_str_a11y_title = false;
        $this->_str_wrapper = false;
        $this->_str_title   = false;
        $this->_str_footer  = false;
    }

    public function setAllowedHTML( $arr = false ) {

        if ( is_array( $arr ) ) {

            $this->_arr_allowed_html = $arr;

            return true;
        }

        return false;
    }

    public function setID( $str = false ) {

        return $this->setString( '_str_id', $str );

    }

    public function setClass( $str = false ) {

        return $this->setString( '_str_class', $str );
    }

    public function setText( $str = false ) {

        return $this->setString( '_str_text', $str );

    }

    public function setIcon( $str = false ) {

        return $this->setString( '_str_icon', $str );

    }

    public function setIconColor( $str = false ) {

        return $this->setString( '_str_icon_color', $str );

    }

    public function setBoolStyle( $bool = true ) {

        return $this->setBool( '_bool_stash_style', $bool );
    }

    public function setA11yID( $str = false ) {

        return $this->setString( '_str_a11y_id', $str );

    }

    public function setA11yClass( $str = false ) {

        return $this->setString( '_str_a11y_class', $str );

    }

    public function setA11yDescribe( $str_content ) {

        $this->_str_a11y = $str_content;
     //   $this->resetPropertyDefaults();
    }


    public function setImgPin( $str = false  ) {

        // TODO - validate?
        return $this->setString( '_str_img_pin', $str );

    }

    public function pushMarker() {

        $this->_arr_stash_markers[] = $this->makeX();
        $this->resetPropertyDefaults();
    }

    public function pushLabel() {

        $this->_arr_stash_labels[] = $this->makeX();
        $this->resetPropertyDefaults();
    }


    public function makeWrapper() {

        if ( $this->_str_a11y !== false ) {
            $this->_bool_a11y = true;
        }

        $this->_str_wrapper = $this->makeX( false, 'figure' );
        $this->resetPropertyDefaults();
    }

    public function makeTitle() {

        if ( $this->_str_text !== false ){
            $this->_str_a11y_title = $this->_str_text;
        }

        $this->_str_title = $this->makeX(true, 'figcaption');
        $this->resetPropertyDefaults();

    }

    public function makeFooter() {

        $this->_str_footer = $this->makeX();
        $this->resetPropertyDefaults();
    }

    protected function makeA11y(){

        if ( $this->_bool_a11y === true ) {
            $this->_bool_a11y = false;
            $this->_str_text  = $this->_str_a11y;
            $this->_str_id    = $this->_str_a11y_id;
            $this->_str_class = $this->_str_a11y_class;
             return $this->makeX();
        }
    }



    /**
     * Pulls the pieces together and spits out the map
     *
     * @return string
     */
    public function getView() {

        $str_ret = '';

        if ( $this->_str_id !== false ) {


            if ( $this->_str_wrapper !== false ) {
                $str_ret .= $this->_str_wrapper;
            }

            if ( $this->_str_title !== false ) {
                $str_ret .= $this->_str_title;
            }


            if ( $this->_str_a11y !== false ) {
             //   $this->_bool_a11y = true;
            }

            // the map!
            $str_ret .= $this->makeX();

            if ( $this->_str_footer !== false ) {
                $str_ret .= $this->_str_footer;
            }

            // if we have an a11y description, it's time to add it
            $str_ret .= $this->makeA11y();

            if ( ! empty( $this->_arr_stash_style ) && $this->_bool_stash_style !== false ) {

                $str_ret .= '<style>';
                $str_ret .= implode( ' ', $this->_arr_stash_style );
                $str_ret .= '</style>';

            }

            if ( $this->_str_wrapper !== false ) {
                $str_ret .= '</figure>'; // wrap close
            }

            $str_markers = implode( '', $this->_arr_stash_markers );
            $str_labels  = implode( '', $this->_arr_stash_labels );

            if ( ! empty( $str_markers ) || ! empty( $str_labels ) ) {

                $str_ret .= '<div style="display: none;">';
                $str_ret .= $str_markers;
                $str_ret .= $str_labels;
                $str_ret .= '</div>';
            }
        }

        // get ready for the next map
        $this->resetMapPropertyDefaults();

        return $str_ret;
    }


    /**
     * @param bool   $bool_close
     * @param string $str_ele_tag
     *
     * @return string
     */
    protected function makeX( $bool_close = true, $str_ele_tag = 'div' ) {

        /*
         * TODO - truth be told, the entire approach to this view needs to be revisited / refactored.
         * It started simple enough and then "evolved" as more features were packed in.
         * Hey. It happens to the best of us. But for now, it works :)
         */

        $str_ret = '';

        $str_ret .= ' <' . esc_attr($str_ele_tag);
        $str_ret .= ' id="' . esc_attr( trim( $this->_str_id ) ) . '"';

        if ( $str_ele_tag == 'figure' && $this->_bool_a11y === true ) {
            if ( $this->_str_a11y_title !== false ){
                $str_ret .= ' title="' . esc_attr( $this->_str_a11y_title ) . '"';
            }

            if ( $this->_str_a11y_id === false ){
                $this->_str_a11y_id = trim( $this->_str_id ) . $this->_str_a11y_id_slug;
            }
            $str_ret .= ' aria-describedby="' . esc_attr( $this->_str_a11y_id ) . '"';
        }

        if ( $this->_str_class !== false ) {
            $str_ret .= ' class="' . esc_attr( $this->_str_class ) . '"';
        }
        $str_ret .= '>';

        if ( $this->_str_text !== false ) {
            $str_ret .= wp_kses( $this->_str_text, $this->_arr_allowed_html );
        } elseif ( $this->_str_icon !== false ) {

            $this->_arr_stash_style[] = $this->mapStyle();

            if ( $this->_str_img_pin !== false ) {
                $str_ret .= file_get_contents( $this->_str_img_pin );
            }
        }

        if ( $bool_close === true ) {
            $str_ret .= '</' . esc_attr($str_ele_tag).'>';
        }

        return $str_ret;

    }


    protected function mapStyle() {

        //   $str_ret .= '<style>';
        $str_ret = '';
        $str_ret .= '#' . esc_attr( trim( $this->_str_id ) ) . ' svg{';
        $str_ret .= 'width:' . esc_attr( $this->_str_icon ) . '; height:' . esc_attr( $this->_str_icon ) . ';';
        // TODO - validate color (in controller?)
        if ( $this->_str_icon_color !== false ) {
            $str_ret .= ' fill:' . esc_attr( $this->_str_icon_color );
        }

        $str_ret .= '}';
        //  $str_ret .= ' #' . esc_attr( trim( $this->_str_id ) ) . ' svg .wpez-osm-svg-outer{';
        //  $str_ret .= 'fill: #ffffff';  // make this set-able?
        //  $str_ret .= '}';

        //  $str_ret .= '</style>';

        return $str_ret;
    }


}
