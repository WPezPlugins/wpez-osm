<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 06-Jun-18
 * Time: 9:31 PM
 */

namespace WPezOSM\App\MVC\Controllers;


class ClassController {

    use \WPezOSM\App\Core\Traits\Set\TraitSetString;

    protected $_str_wrap_slug;
    protected $_str_sc_name_a11y;

    protected $_arr_markers;
    protected $_arr_labels;
    protected $_arr_maps;

    // protected $_int_count_map;
    protected $_int_count_id;

    protected $_str_id_slug_marker;
    protected $_str_id_slug_label;
    protected $_str_id_slug_map;

    // protected $_str_current_id;
    protected $_float_current_lon;
    protected $_float_current_lon_off;
    protected $_float_current_lat;
    protected $_float_current_lat_off;

    protected $_float_map_current_lon;
    protected $_float_map_current_lat;
    protected $_int_map_current_zoom;

    protected $_new_view;
    protected $_arr_marker_icon_sizes;
    protected $_bool_screen_reader_text_style;

    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_str_wrap_slug    = '-wrap';
        $this->_str_sc_name_a11y = false;

        $this->_arr_markers = [];
        $this->_arr_labels  = [];
        $this->_arr_maps    = [];

        $this->_int_count_id = 0;
        // $this->_int_count_map = 0;

        $str = 'wpez-osm'; // TODO - config?

        $this->_str_id_slug_marker = $str . '-marker-'; // set in config
        $this->_str_id_slug_label  = $str . '-label-'; // set in config
        $this->_str_id_slug_map    = $str . '-map-'; // set in config

        // $this->_str_current_id = false;
        $this->_float_current_lon     = false;  // use config default?
        $this->_float_current_lon_off = 0;  //
        $this->_float_current_lat     = false; // use config default?
        $this->_float_current_lat_off = 0;

        $this->_float_map_current_lon = 0;  // use config default?
        $this->_float_map_current_lat = 0; // use config default?

        $this->_int_map_current_zoom = 2;

        $this->_arr_marker_icon_sizes = [
            'xs' => '25px',
            'sm' => '30px',
            'md' => '35px',
            'lg' => '42px',
            'xl' => '50px'
        ];

        $this->_bool_screen_reader_text_style = true;
    }

    public function setShortcodeNameA11y( $str = false ) {

        return $this->setString( '_str_sc_name_a11y', $str );
    }


    public function setView( $obj = false ) {

        if ( method_exists( $obj, 'getView' ) ) {

            $this->_new_view = $obj;

            return true;
        }

        return false;
    }


    //
    public function setMarkerIconSizes( $arr = false ) {

        if ( is_array( $arr ) ) {
            $this->_arr_marker_icon_sizes = $arr;

            return true;
        }

        return false;
    }

    public function getMaps() {

        return $this->_arr_maps;
    }


    public function getScreenReaderTextStyle() {

        return $this->_bool_screen_reader_text_style;
    }

    /*
    public function scArgs( $args ) {

        return 'TODO';
    }
    */


    protected function attsDefaultsMarker() {

        $arr = [
            'id'         => false,
            'class'      => 'wpez-osm-marker',
            'lon'        => false,
            'lat'        => false,
            'icon'       => 'md',
            'icon_color' => false,
        ];

        return $arr;

    }

    protected function attsDefaultsLabel() {

        $arr = [
            'id'               => false,
            'class'            => 'wpez-osm-label',
            'lon'              => false,
            'lon_off'          => false,
            'lat'              => false,
            'lat_off'          => false,
            'text'             => false,
            'color'            => 'TODO',
            'background_color' => 'TODO'
        ];

        return $arr;

    }

    protected function attsDefaultsA11y() {

        $arr = [
            'id'                       => false,
            'class'                    => false,
            'screen_reader_text_style' => true
        ];

        return $arr;
    }

    protected function attsDefaultsMap() {

        $arr = [
            'id'           => false,
            'class'        => 'wpez-osm-map',
            'lon'          => false,
            'lat'          => false,
            'zoom'         => false,
            'full_screen'  => true,
            'height'       => 'TODO',
            'title'        => false,  // how does adding html tag to title effect it's a11y use?
            'title_class'  => 'TODO',
            'footer'       => false,
            'footer_class' => 'TODO',
            'style'        => true
        ];

        return $arr;

    }


    public function scOSM( $arr_sc_atts, $str_content = false, $str_sc_name = false ) {

        if ( $str_sc_name == $this->_str_sc_name_a11y && ! empty( $str_content ) ) {

            $this->thisA11y( $arr_sc_atts, $str_content );

        } elseif ( isset( $arr_sc_atts['this'] ) ) {

            switch ( $arr_sc_atts['this'] ) {

                case 'marker':
                    return $this->thisMarker( $arr_sc_atts );

                case 'label':
                    return $this->thisLabel( $arr_sc_atts );

                case 'map':
                default:
                    return $this->thisMap( $arr_sc_atts );
            }
        }

        return '';
    }

    protected function thisA11y( $arr_sc_atts, $str_content ) {

        $arr_atts = shortcode_atts( $this->attsDefaultsA11y(), $arr_sc_atts );

        if ( $arr_atts['id'] !== false ) {
            $this->_new_view->setA11yID( $arr_atts['id'] );
        }

        if ( $arr_atts['class'] !== false ) {
            $this->_new_view->setA11yClass( $arr_atts['class'] );
        }

        if ( $arr_atts['screen_reader_text_style'] === 'false' ) {
            $this->_bool_screen_reader_text_style = false;
        }

        $this->_new_view->setA11yDescribe( $str_content );
    }


    protected function thisMarker( $arr_sc_atts ) {

        $arr_atts = shortcode_atts( $this->attsDefaultsMarker(), $arr_sc_atts );

        // the first marker has to have a lon and lat. after that we'll use the current_* as the default.

        if ( $this->_int_count_id == 0 && ( $arr_atts['lon'] === false || $arr_atts['lat'] === false ) ) {
            return '<!-- marker - lon or lat missing -->';
        }

        if ( $arr_atts['lon'] === false ) {
            if ( $this->_float_current_lon !== false ) {
                $arr_atts['lon'] = floatval( $this->_float_current_lon );
            } else {
                return '<!-- marker - lon missing && current_lon is false -->';
            }
        } else {
            $arr_atts['lon'] = floatval( $arr_atts['lon'] );
        }
        $this->_float_current_lon = $arr_atts['lon'];

        if ( $arr_atts['lat'] === false ) {
            if ( $this->_float_current_lat !== false ) {
                $arr_atts['lat'] = floatval( $this->_float_current_lat );
            } else {
                return '<!-- marker - lat missing && current_lon is false -->';
            }
        } else {
            $arr_atts['lat'] = floatval( $arr_atts['lat'] );
        }
        $this->_float_current_lat = $arr_atts['lat'];

        $arr_atts['id']   = $this->makeID( $arr_atts['id'], $this->_str_id_slug_marker );
        $arr_atts['icon'] = $this->markerIcon( $arr_atts['icon'] );

        $this->_new_view->setID( $arr_atts['id'] );
        if ( $arr_atts['class'] != 'false' ) {
            $this->_new_view->setClass( $arr_atts['class'] );
        }

        if ( $arr_atts['icon'] !== 'false' ) {
            $this->_new_view->setIcon( $arr_atts['icon'] );
            if ( $arr_atts['icon_color'] !== false ) {
                $this->_new_view->setIconColor( $arr_atts['icon_color'] );
            }
        }
        $this->_new_view->pushMarker();
        $this->_arr_markers[] = $arr_atts;

    }

    protected function markerIcon( $str = false ) {

        if ( $str == false || $str == 'false' ) {
            return false;
        }

        if ( isset( $this->_arr_marker_icon_sizes[ $str ] ) ) {
            return $this->_arr_marker_icon_sizes[ $str ];
        }

        return '35px'; // TODO - add to config?
    }


    protected function makeID( $mix, $slug ) {

        // TODO - add check for valid id
        if ( $mix === false ) {
            $this->_int_count_id++;

            return $slug . trim( $this->_int_count_id );
        }

        return $mix;
    }

    protected function thisLabel( $arr_sc_atts ) {

        $arr_atts = shortcode_atts( $this->attsDefaultsLabel(), $arr_sc_atts );

        if ( $arr_atts['text'] !== false ) {

            $arr_atts['id'] = $this->makeID( $arr_atts['id'], $this->_str_id_slug_label );

            // -- lon
            if ( $arr_atts['lon'] === false ) {
                if ( $this->_float_current_lon !== false ) {
                    $arr_atts['lon'] = floatval( $this->_float_current_lon );
                } else {
                    return '<!-- label - lon missing && current_lon is false -->';
                }
            } else {
                $arr_atts['lon'] = floatval( $arr_atts['lon'] );
            }
            // lon_off
            if ( $arr_atts['lon_off'] !== false ) {
                $this->_float_current_lon_off = floatval( $arr_atts['lon_off'] );
            }
            // lon - with lon_off adjustment
            $arr_atts['lon'] = $this->_float_current_lon_off + $arr_atts['lon'];

            // -- lat
            if ( $arr_atts['lat'] === false ) {
                if ( $this->_float_current_lat !== false ) {
                    $arr_atts['lat'] = floatval( $this->_float_current_lat );
                } else {
                    return '<!-- label - lat missing && current_lat is false -->';
                }
            } else {
                $arr_atts['lat'] = floatval( $arr_atts['lat'] );
            }
            // lat_off
            if ( $arr_atts['lat_off'] !== false ) {
                $this->_float_current_lat_off = floatval( $arr_atts['lat_off'] );
            }

            // lat - with lat_off adjustment
            $arr_atts['lat'] = $this->_float_current_lat_off + $arr_atts['lat'];

            // we don't need these for the front end (wp_localize_script) but then again ya never know so they
            // we (temporarily) stored in the $arr_atts
            unset( $arr_atts['lon_off'] );
            unset( $arr_atts['lat_off'] );

        } else {
            return '<!-- label - no label arg specified -->';
        }

        $this->_new_view->setID( $arr_atts['id'] );
        if ( $arr_atts['class'] != 'false' ) {
            $this->_new_view->setClass( $arr_atts['class'] );
        }
        $this->_new_view->setText( $arr_atts['text'] );

        $this->_new_view->pushLabel();
        $this->_arr_labels[] = $arr_atts;
    }

    protected function thisMap( $arr_sc_atts, $str_content = false ) {

        $arr_atts = shortcode_atts( $this->attsDefaultsMap(), $arr_sc_atts );

        if ( $arr_atts['lon'] === false ) {

            $arr_atts['lon'] = floatval( $this->_float_current_lon );
        } else {
            $this->_float_current_lon = $arr_atts['lon'] = floatval( $arr_atts['lon'] );
        }

        if ( $arr_atts['lat'] === false ) {

            $arr_atts['lat'] = floatval( $this->_float_current_lat );

        } else {
            $this->_float_current_lat = $arr_atts['lat'] = floatval( $arr_atts['lat'] );
        }

        if ( $arr_atts['zoom'] === false ) {
            $arr_atts['zoom'] = absint( $this->_int_map_current_zoom );
        } else {
            $this->_int_map_current_zoom = $arr_atts['zoom'] = absint( $arr_atts['zoom'] );
        }

        $arr_atts['id'] = $this->makeID( $arr_atts['id'], $this->_str_id_slug_map );

        // add the markers to the current map and reset the markers
        $arr_atts['markers'] = $this->_arr_markers;
        $this->_arr_markers  = [];

        // add the labels to the current map...and reset
        $arr_atts['labels'] = $this->_arr_labels;
        $this->_arr_labels  = [];

        // add the map to the maps[]. this will eventually be used by wp_localize_script(). this
        // array becomes the data for our .js that in turn uses openlayer.js
        $this->_arr_maps[] = $arr_atts;


        // title
        if ( is_string( $arr_atts['title'] ) ) {
            $this->_new_view->setID( $arr_atts['id'] . '-title' . $this->_str_wrap_slug );
            if ( $arr_atts['class'] != 'false' ) {
                $this->_new_view->setClass( $arr_atts['class'] . '-title' . $this->_str_wrap_slug );
            }
            $this->_new_view->setText( $arr_atts['title'] );
            $this->_new_view->MakeTitle();
        }

        // footer
        if ( is_string( $arr_atts['footer'] ) ) {
            $this->_new_view->setID( $arr_atts['id'] . '-footer' . $this->_str_wrap_slug );
            if ( $arr_atts['class'] != 'false' ) {
                $this->_new_view->setClass( $arr_atts['class'] . '-footer' . $this->_str_wrap_slug );
            }
            $this->_new_view->setText( $arr_atts['footer'] );
            $this->_new_view->MakeFooter();
        }

        // wrapper - MUST follow title
        $this->_new_view->setID( $arr_atts['id'] . $this->_str_wrap_slug );
        if ( $arr_atts['class'] != 'false' ) {
            $this->_new_view->setClass( $arr_atts['class'] . $this->_str_wrap_slug );
        }
        $this->_new_view->MakeWrapper();

        // and now for the map itself
        $this->_new_view->setID( $arr_atts['id'] );
        if ( $arr_atts['class'] != 'false' ) {
            $this->_new_view->setClass( $arr_atts['class'] );
        }

        if ( $arr_atts['style'] === 'false' ) {
            $this->_new_view->setBoolStyle( false );
        }

        return $this->_new_view->getView();

    }

}