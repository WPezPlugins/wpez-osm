window.onload = function () {
    if (typeof wpezOSM !== 'undefined') {
        var intLenArr = wpezOSM.length;
        for (i = 0; i < intLenArr; i++) {

            var map = makeMap(wpezOSM[i].id, wpezOSM[i].lon, wpezOSM[i].lat, wpezOSM[i].zoom, wpezOSM[i].full_screen);

            if (typeof wpezOSM[i].markers !== 'undefined') {
                makeOverlays(map, wpezOSM[i].markers, 'marker');
            }
            if (typeof wpezOSM[i].labels !== 'undefined') {
                makeOverlays(map, wpezOSM[i].labels, 'label');

            }
        }
    } else {
        console.log('wpesOSM - something has gone wrong.');
    }


    function makeMap(id, lon, lat, zoom, full_screen) {

        var x = new ol.source.OSM();

        var layer = new ol.layer.Tile({
            source: x
        });

        var pos = ol.proj.fromLonLat([lon, lat]);

        var fs = ol.control.defaults({
            attributionOptions: {
                // className: 'hide-this-shit',
                collapsible: false
            }
        });
        if (full_screen != 'false') {
            fs = fs.extend([new ol.control.FullScreen()]);
        }

        // this essentially removes the openlayers logo (sorry).
        var logoElement = document.createElement('i');

        var map = new ol.Map({
            layers: [layer],
            target: id,
            controls: fs,
            logo: logoElement,
            view: new ol.View({
                center: pos,
                zoom: zoom
                // map* - TODO - other essential args?
            })
        });
        return map;
    }

    function makeOverlays(map, overlays, make) {

        var intLenArr = overlays.length;
        var i;
        for (i = 0; i < intLenArr; i++) {

            if (make == 'marker') {
                makeMarker(map, overlays[i].id, overlays[i].lon, overlays[i].lat);
            } else if (make == 'label') {
                makeLabel(map, overlays[i].id, overlays[i].lon, overlays[i].lat);
            }

        }
    }


    function makeMarker(map, id, lon, lat) {

        var pos = ol.proj.fromLonLat([lon, lat]);

        var marker = new ol.Overlay({
            // marker* - marker_lon
            // marker* - marker_lat
            position: pos,
            // marker* - marker_positioning << TODO - what is this?
            positioning: 'center-center',
            // marker* - marker_id
            element: document.getElementById(id),
            stopEvent: false  // << TODO - what is this?
        });
        map.addOverlay(marker);

    }

    function makeLabel(map, id, lon, lat) {

        var pos = ol.proj.fromLonLat([lon, lat]);

        var label = new ol.Overlay({
            // marker* - marker_label_lon
            // marker* - marker_label_lat
            position: pos,
            // marker* - marker_label_id
            element: document.getElementById(id)
        });
        map.addOverlay(label);
    }
}