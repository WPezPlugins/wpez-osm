<?php

namespace WPezOSM\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezOSM\App\Core\Hooks\Register\ClassRegister as HooksRegister;
// use WPezOSM\App\Setup\Hooks\ClassHooks;
use WPezOSM\App\Setup\Shortcode\ClassShortcode;
use WPezOSM\App\MVC\Views\ClassView;
use WPezOSM\App\MVC\Controllers\ClassController;
use WPezOSM\App\Setup\Scripts\ClassScripts;
use WPezOSM\App\Setup\Styles\ClassStyles;

class ClassPlugin {

    protected $_arr_actions;
    protected $_arr_filters;

    protected $_new_config;
    // protected $_new_hooks;
    protected $_new_sc;
    protected $_new_controller;
    protected $_new_scripts;
    protected $_new_styles;


    public function __construct() {

        $this->setPropertyDefaults();

        $this->addShortcode();

        $this->addActions();

        $this->addFilters();

        $this->hooksRegister();
    }


    protected function setPropertyDefaults() {

        $this->_arr_actions = [];
        $this->_arr_filters = [];

        $this->_new_config = new ClassConfig();

        // $this->_new_hooks = new ClassHooks();

        $new_view = new ClassView();
        $new_view->setAllowedHTML( $this->_new_config->allowed_html );
        $new_view->setImgPin( $this->_new_config->img_pin );

        $this->_new_controller = new ClassController();
        $this->_new_controller->setShortcodeNameA11y($this->_new_config->sc_name . $this->_new_config->a11y_slug );
        $this->_new_controller->setMarkerIconSizes($this->_new_config->marker_icon_sizes);
        $this->_new_controller->setView( $new_view );

        $this->_new_sc = new ClassShortcode();

        $this->_new_scripts = new ClassScripts();
        $this->_new_scripts->setScripts( $this->_new_config->scripts);

        $this->_new_styles = new ClassStyles();
        $this->_new_styles->setStyles( $this->_new_config->styles );

    }

    public function addShortcode() {

        $this->_new_sc->setShortcodeMethod( $this->_new_config->sc_method );
        $this->_new_sc->setShortcodeName( $this->_new_config->sc_name );
        $this->_new_sc->setA11ySlug( $this->_new_config->a11y_slug );
        $this->_new_sc->setController( $this->_new_controller );

    }


    protected function hooksRegister() {

        $new = new HooksRegister();

        $new->loadActions( $this->_arr_actions );

        $new->loadFilters( $this->_arr_filters );

        $new->doRegister();
    }


    protected function addActions() {

        $this->_arr_actions[] = [
            'hook'      => 'init',
            'component' => $this->_new_sc,
            'callback'  => 'addShortcode',
            'priority'  => 25
        ];

        $this->_arr_actions[] = [

            'hook'      => 'wp_enqueue_scripts',
            'component' => $this,
            'callback'  => 'hasShortcode',
            'priority'  => 15
        ];

        $this->_arr_actions[] = [

            'hook'      => 'wp_enqueue_scripts',
            'component' => $this->_new_scripts,
            'callback'  => 'wpRegisterScript',
            'priority'  => 30
        ];

        $this->_arr_actions[] = [

            'hook'      => 'wp_enqueue_scripts',
            'component' => $this->_new_scripts,
            'callback'  => 'wpEnqueueScript',
            'priority'  => 35
        ];

        $this->_arr_actions[] = [

            'hook'      => 'wp_enqueue_scripts',
            'component' => $this->_new_styles,
            'callback'  => 'wpRegisterStyle',
            'priority'  => 30
        ];

        $this->_arr_actions[] = [

            'hook'      => 'wp_enqueue_scripts',
            'component' => $this->_new_styles,
            'callback'  => 'wpEnqueueStyle',
            'priority'  => 35
        ];

        $this->_arr_actions[] = [
            'hook'      => 'get_footer',
            'component' => $this,
            'callback'  => 'wpLocalizeScript',
            'priority'  => 20
        ];

        $this->_arr_actions[] = [
            'hook'      => 'get_footer',
            'component' => $this->_new_scripts,
            'callback'  => 'wpLocalizeScript',
            'priority'  => 100
        ];

        $this->_arr_actions[] = [
            'hook'      => 'get_footer',
            'component' => $this,
            'callback'  => 'screenReaderTextStyle',
            'priority'  => 30
        ];
    }


    /**
     * if there's no shortcode then we won't load the scripts or styles
     */
    public function hasShortcode() {

        global $post;
        if ( is_a( $post, 'WP_Post' ) && ! has_shortcode( $post->post_content, $this->_new_config->sc_name ) ) {
            $this->_new_scripts->setActive( false );
            $this->_new_styles->setActive( false );
        }

    }

    // TODO - move (these couple function out of $this
    public function screenReaderTextStyle() {

        if ( $this->_new_controller->getScreenReaderTextStyle() === true ){
            $str_ret = $this->_new_styles->classScreenReaderText();
            if ( $str_ret !== false ){
                echo '<style>' . $str_ret . '</style>';
            }
        }
    }


    /**
     * The arr_maps from the controller needs to be passed to the scripts
     */
    public function wpLocalizeScript() {

        $arr = $this->_new_controller->getMaps();
        $this->_new_scripts->setLocalizeScript( 'wpez_osm', 'wpezOSM', $arr );

    }


    protected function addFilters() {

        $this->_arr_filters[] = (object)[
            'hook'      => 'wp_default_editor',
            'component' => $this,
            'callback'  => 'editorForceTextTab',
            'priority'  => 75
        ];
    }


    public function editorForceTextTab( $tab ) {

        return 'html';
    }


}