<?php
/**
 * Created by PhpStorm.
 */


namespace WPezOSM\App\Setup\Styles;

class ClassStyles {


    protected $_bool_active;
    protected $_arr_styles;

    public function __construct() {

        $this->setPropertyDefaults();

    }


    public function setPropertyDefaults() {

        $this->_bool_active = false;

        $this->_arr_styles = [];

    }


    public function setStyles( $arr = false ) {

        if ( is_array( $arr ) ) {
            $this->_arr_styles  = $arr;
            $this->_bool_active = true;

            return true;
        }

        return false;
    }

    public function setActive( $bool ) {

        $this->_bool_active = (boolean)$bool;
    }

    public function wpRegisterStyle() {

        if ( $this->_bool_active !== true ) {
            return;
        }

        foreach ( $this->_arr_styles as $arr ) {

            $obj = (object)$arr;

            wp_register_style(
                $obj->handle,
                $obj->src,
                $obj->deps,
                $obj->ver,
                $obj->media
            );
        }
    }

    public function wpEnqueueStyle() {

        if ( $this->_bool_active !== true ) {
            return;
        }

        foreach ( $this->_arr_styles as $arr ) {

            $obj = (object)$arr;

            wp_enqueue_style(
                $obj->handle
            );
        }
    }

    /**
     * ref: https://make.wordpress.org/accessibility/2015/02/09/hiding-text-for-screen-readers-with-wordpress-core/
     *
     * @return bool|string
     */
    public function classScreenReaderText(){

        // TODO - make this into a trait?

        if ( $this->_bool_active !== true ) {
            return false;
        }

        $str_ret = '';

        $str_ret .= '.screen-reader-text {';
        $str_ret .= ' border: 0;';
        $str_ret .= ' clip: rect(1px, 1px, 1px, 1px);';
        $str_ret .= ' clip-path: inset(50%);';
        $str_ret .= ' height: 1px;';
        $str_ret .= ' margin: -1px;';
        $str_ret .= ' overflow: hidden;';
        $str_ret .= ' padding: 0;';
        $str_ret .= ' position: absolute !important;';
        $str_ret .= ' width: 1px;';
        $str_ret .= ' word-wrap: normal !important;';
        $str_ret .= '}';
        
        return $str_ret;

    }


}