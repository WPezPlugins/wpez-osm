<?php
/**
 * Created by PhpStorm.
 */

namespace WPezOSM\App\Setup\Shortcode;

class ClassShortcode {

    use \WPezOSM\App\Core\Traits\Set\TraitSetString;

    protected $_new_controller;

    protected $_str_method;
    protected $_str_name;
    protected $_str_a11y_slug;


    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        $this->_new_controller = false;
        $this->_str_method    = false;
        $this->_str_name     = false;
        $this->_str_a11y_slug = false;

    }

    public function setShortcodeMethod( $str = false  ) {

        return $this->setString('_str_method', $str);
    }

    public function setShortcodeName( $str = false ) {

        return $this->setString('_str_name', $str);
    }

    public function setA11ySlug( $str = false ) {

        return $this->setString('_str_a11y_slug', $str);
    }

    public function setController( $obj ) {

        if ( is_object($obj )) {
            $this->_new_controller = $obj;
            return true;
        }
        return false;
    }


    public function addShortcode() {

        if ( $this->_new_controller !== false ) {

            if ( method_exists($this->_new_controller, $this->_str_method ) ) {
                add_shortcode( $this->_str_name, [ $this->_new_controller, $this->_str_method ] );
                add_shortcode( $this->_str_name . $this->_str_a11y_slug, [ $this->_new_controller, $this->_str_method ] );
                return true;
            }
        }
        return false;
    }
}