<?php
/**
 * Created by PhpStorm.
 */


namespace WPezOSM\App\Setup\Scripts;

class ClassScripts {


    protected $_bool_active;
    protected $_str_localize_handle;
    protected $_str_localize_name;
    protected $_arr_localize_data;
    protected $_arr_scripts;

    public function __construct() {

        $this->setPropertyDefaults();

    }


    public function setPropertyDefaults() {

        $this->_bool_active = false;

        $this->_str_localize_handle = false;
        $this->_str_localize_name = false;
        $this->_arr_localize_data    = false;
        $this->_arr_scripts = [];

    }


    public function setScripts( $arr = false ) {

        if ( is_array( $arr ) ) {
            $this->_arr_scripts = $arr;
            $this->_bool_active = true;
            return true;
        }

        return false;
    }


    public function setLocalizeScript( $str_handle = false, $str_name = false, $arr_data = false ) {

        if ( is_string($str_handle) && is_string($str_name) && is_array( $arr_data ) ) {
            $this->_str_localize_handle = $str_handle;
            $this->_str_localize_name = $str_name;
            $this->_arr_localize_data = $arr_data;
        }
    }



    public function setActive($bool){

        $this->_bool_active = (boolean)$bool;
    }


    public function wpRegisterScript() {

        if ( $this->_bool_active !== true ){
            return;
        }

        foreach ( $this->_arr_scripts as $arr ) {

            $obj = (object)$arr;

            wp_register_script(
                $obj->handle,
                $obj->src,
                $obj->deps,
                $obj->ver,
                $obj->in_footer
            );
        }
    }

    public function wpEnqueueScript() {

        if ( $this->_bool_active !== true ){
            return;
        }

        foreach ( $this->_arr_scripts as $arr ) {

            $obj = (object)$arr;

            wp_enqueue_script(
                $obj->handle
            );
        }
    }

    public function enqueueInline() {

        // TODO - do we want to do this?
    }

    public function wpLocalizeScript() {

        if ( $this->_bool_active !== true ){
            return;
        }

        wp_localize_script( $this->_str_localize_handle, $this->_str_localize_name, $this->_arr_localize_data );

    }

}