<?php
namespace WPezOSM\App\Core\Traits\Set;

trait TraitSetArray {

    protected function setArray( $str_prop = false, $arr = false, $int_min_count = 0 ) {

        if ( property_exists( $this, $str_prop ) && is_array( $arr ) && count($arr) >= absint($int_min_count)) {

            $this->$str_prop = $arr;

            return true;
        }
        return false;
    }
}