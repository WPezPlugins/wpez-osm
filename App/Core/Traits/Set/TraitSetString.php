<?php

namespace WPezOSM\App\Core\Traits\Set;

trait TraitSetString {

    protected function setString( $str_prop = false, $str = false, $int_min_len = 0 ) {

        if ( property_exists( $this, $str_prop ) && is_string( $str ) && strlen($str) >= absint($int_min_len)) {

            $this->$str_prop = $str;

            return true;
        }
        return false;
    }
}