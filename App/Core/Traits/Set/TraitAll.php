<?php
namespace WPezOSM\App\Core\Traits\Set;

trait TraitAll {

    use \WPezOSM\App\Core\Traits\Set\TraitSetArray;
    use \WPezOSM\App\Core\Traits\Set\TraitSetBool;
    use \WPezOSM\App\Core\Traits\Set\TraitSetString;

}