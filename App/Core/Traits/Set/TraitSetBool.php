<?php

namespace WPezOSM\App\Core\Traits\Set;

trait TraitSetBool {

    protected function setBool( $str_prop = false, $bool = [], $bool_cast = true ) {

        if ( property_exists( $this, $str_prop ) ) {

            if ( $bool_cast === true ) {

                $this->$str_prop = (boolean)$bool;

                return true;
            } elseif ( is_bool( $bool ) ) {
                $this->$str_prop = $bool;

                return true;
            }

            return false;


        }
    }
}