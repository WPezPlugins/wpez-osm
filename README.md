## WPezPlugins: WPezOSM

__A WordPress plugin that uses two shortcodes to render OpenStreetMap.org maps using the OpenLayers.org JavaScript library.__

In addition, there is an attempt to make the maps A11Y-friendly (but the validity of that effort has yet to be verified by an accessibility expert).

In short:

- Uses Open Street Maps and Open Layers. 

- All JavaScript, no jQuery.

- Simply and ez to use.

- Clean code. Well, once the ClassView gets refactored :)

- Powerful and flexible but far short of feature bloat.


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

In addition to a map you can add markers (aka pins) and labels for those markers. All of these via WordPress shortcodes. A map can have a title, a footer, as well as an aria-describedBy description.
 
Disclaimer: If you're looking for a map for tons of markers and other special features this is __not__ the plugin you're looking for.

The emphasis here is on a straightforward privacy-centic map tool, with a handful of cool features, but nothing excessive or bloat-y. While WPezOSM can do multiple markers + labels the target use case is someone wanting to add a map to a page to display the location of a business or event. It can also be used in blog article if you have content in need to map support.

Issues, bugs and/or feature requests should be submitted via GitLab's Issues page for this repo. 

> --
>
> IMPORTANT
>
> You will need to know know the lon and lat values for the address(es) you want to map. 
>
> https://www.latlong.net/convert-address-to-lat-long.html
>
> -- 



### GETTING STARTED

As mentioned there are two shortcodes: [wpez_osm] (no closing tag necessary) for markers, labels and maps, as well as [wpez_osm_a11y]...[/wpez_osm_a11y] for the aria-describedBy (closing tag required).

##### 1 - Markers, Labels, Maps atts - [wpez_osm this="?"...]

The valid shortcode atts are:

- __this__ | OPTIONAL | Default: "map" - this tells the plugin what the rest of the shortcode is going to define. Valid values are: "marker", "label" and "map"

- __id__ | OPTIONAL | Default: "wpez-osm-{value of __this__}-#" (e.g., wpez-osm-marker-3) - If you don't like the default CSS id the plugin provides you can override the default with your own value for this id. The number that's appended to the default is auto-incremented each time an ID is created, regardless of the this="*" type. Obviously, you already know the id should be unique across the entire page. When in doubt, don't add your own id. 

- __class__ | OPTIONAL | Default: "wpez-osm-{value of __this__} (e.g., wpez-owm-label) - If you don't like the default CSS class the plugin provides you can override the default with your own value. Also, you can set the value to ="false" and the default class won't be rendered. That is, "false" turns off adding the class="..." to the element. When in doubt, don't add your own class.

- __lon__ | REQUIRED then OPTIONAL | Default: none - Short for longitude. lon will be a decimal number (in quotes); negatives are possible and valid. Once you set the lon in the first marker that lon value will become the default for any label, marker or map that follows; until you change the lon. If this explanation isn't clear, the examples (below) should help. 

- __lat__ | REQUIRED then OPTIONAL | Default: none - Short for latitude. lat has the same cascading default value as lon.


##### Marker Only atts - [wpez_osm this="marker"...]

- __icon__ | OPTIONAL | Default: "md" (medium)- If you don't want the pin icon to be rendered set the value to "false", else valid (size) values are "xs", "sm", "md", "lg", "xl".

- __icon_color__ | OPTIONAL | Default: "#000000" (black) - Color of the icon in #hex, or standard markup string-based colors (e.g., "red", "blue", "green", etc.)
 
 
##### Label Only ats - For [wpez_osm this="label"...]

- __lon_off__ | OPTIONAL | Default: 0 - Short for longitude offset. This decimal number (in quotes) will be added to the lon to nudge the label away from the marker. Note: If this number is too large you can nudge the label off the map. You can use negative value for lon_off.

- __lon_off__ | OPTIONAL | Default: 0 - Short for latitude offset. 

- __text__ | REQUIRED | Default: none - The text for the label. If text is not set or is blank, the label will not be rendered. In addition, the following html tags are permitted: < br > (aka return), < em > ...< /em >, < strong >...< /strong >, < b >...< /b >. _Note: Spaces have been added to prevent the actual tags from being rendered in this doc._


##### Map only atts - For [wpez_osm this="map"...]

- __zoom__ | OPTIONAL | Default: 2 - zoom will be an integer in quotes.  
   
- __full_screen__ | OPTIONAL | Default: true - Set to "false" if you don't want the expect to full screen icon to be added to the map.

- __title__ | OPTIONAL | Default: none -  The title will appear above the map. It has the same allowed html tags as label's text. This value is also used for accessibility, so it's recommended you have a title.

- __footer__ | OPTIONAL | Default: true - The footer will appear below the map. 

- __style__ | OPTIONAL | Default: true - Set to "false" if you don't want the plugin to add the inline CSS for this map (and you intend on adding your own CSS). _Note: Marker icons will "disappear" if you surpress the CSS._


##### 2 - A11Y atts - [wpez_osm this="a11y"...]

- __id__ | OPTIONAL | Default: "wpez-osm-{value of __this__}-#-wrap-a11y" (e.g., wpez-osm-map-4-wrap-a11y)

- __class__ | OPTIONAL | Default: "screen-reader-text"  

- __screen_reader_text_style__ | OPTIONAL | Default: true - Set this to "false" if you don't want the plugin to add CSS snippet for .screen-reader-text. Note: The map's style="false" will also cause this snippet to _not_ be rendered.


### Example

[wpez_osm this="marker" lon="-0.1281" lat="51.508" icon="xl" icon_color="red"]

[wpez_osm this="label" lon_off=".00015" lat_off="0.0002" text="Trafalgar Square<br>London WC2N 5DN"]

[wpez_osm_a11y]This is my A11Y describedBy for this map. showing Trafalgar Square, London WC2N 5DN[/wpez_osm_a11y]

[wpez_osm this="map" zoom="18" full_screen="true" title="This is my map title" footer="And this is my map footer"]


What you need to know about the example:

1 - Most importantly: The order matters. The plugin works top to bottom. That is, it adds markers and labels (and the wpez_osm_a11y) until it hits a this="map". Everything above that map is considered for that map. I hope that makes sense.

2 - If you have a second map, the lon and lat will carry forward at the marker and map levels. That is, the second map will use the  previous map's center, but you can add different lon + lat markers. 

2 - The shortcodes here are listed line by line for clarity. You're going to want to remove returns and spaces, as is typical of all WP shortcodes.

3 - Notice how the marker sets the lon and lat and the map uses that for its center. If that's not what you need, you can have a different center for the map if necessary. The cascading lon + lat helps save time, or just override it.

4 - Only the [wpez_osm_a11y] has a closing [/wpez_osm_a11y].  

That's it. A Google-free map done The ezWay. 

### FAQ

__1 - Can you have multiple maps on a page?__

Yup.

__2 - Can you add Feature X?__

Please use the repo's Issue section for such suggestions and requests. 

__3 - How can I add a map programmatically?__

Trying using the WordPress do_shortcode() function. https://developer.wordpress.org/reference/functions/do_shortcode/


### HELPFUL LINKS

- https://OpenStreetMap.org

- https://OpenLayers.org

- https://www.latlong.net/convert-address-to-lat-long.html

- http://petercollingridge.appspot.com/svg-optimiser

 
 
### TODO

- Revisit ClassView (as it sprawled and mutated quickly once the PoC was working)

- Add an atts for map height

- Add an atts to prevent the zoom +/- from being displayed

- Add color and background-color for labels

- Get an accessibility / aria / A11Y expert to have a look

- Strip the markup tags from the title= that's in the wrapper (for accessibility purposes)

- Use a custom version of the OL.js (so that loads faster and without bloat)



### CHANGE LOG

__-- 0.0.1__

- INIT - And away we go...
   